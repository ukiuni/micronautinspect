package example.controller;

import io.micronaut.http.annotation.*;
import example.repository.GenreRepository;
import example.model.Genre;

@Controller("/hello")
public class HelloController {
    protected final GenreRepository genreRepository;
    public HelloController(GenreRepository repository){
        this.genreRepository = repository;
    }

    @Get(uri = "/{name}")
    public Genre hello(String name) {
        return genreRepository.save(name);
    }
}