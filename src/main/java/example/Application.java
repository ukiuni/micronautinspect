package example;

import io.micronaut.runtime.Micronaut;

import io.reactivex.Maybe;
import io.reactivex.Single;
import io.micronaut.http.annotation.*;
import java.util.List;

public class Application {

    public static void main(String[] args) {
        Micronaut.run(Application.class);
    }
}
