package example;

import io.micronaut.http.annotation.Get;
import io.micronaut.http.client.annotation.Client;
import io.reactivex.Single;

import javax.validation.constraints.NotBlank;
import example.model.Genre;

@Client("/hello")
public interface HelloClient {
    @Get("/{name}")
    public Single<Genre> hello(@NotBlank String name);
}