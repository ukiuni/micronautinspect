package example;

import static org.junit.jupiter.api.Assertions.assertEquals;

import io.micronaut.test.annotation.MicronautTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.Random;

@MicronautTest
public class HelloTest {

    @Inject
    HelloClient helloClient;

    @Test
    void testHello() {
        String name = "name:" + new Random().nextInt(10000);
        assertEquals(
                name,
                helloClient.hello(name).blockingGet().getName());
    }
}
